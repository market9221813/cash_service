package postgres

import (
	"context"
	"fmt"
	"cash_service/config"
	"cash_service/storage"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	DB *pgxpool.Pool
	cfg config.Config
}

func New (ctx context.Context, cfg config.Config) (storage.IStorage, error){
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil{
		fmt.Println("Error while parsing to pool config!",err.Error())
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil{
		fmt.Println("Error while creating a new pool!",err.Error())
		return Store{},err
	}
 	
	return Store {
		DB: pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close()  {
	s.DB.Close()
}

func (s Store) Shift() storage.IShiftStorage {
	return NewShiftRepo(s.DB)
}

func (s Store) Sale() storage.ISaleStorage {
	return NewSaleRepo(s.DB)
}

func (s Store) SaleProduct() storage.ISaleProductStorage {
	return NewSaleProductRepo(s.DB)
}
