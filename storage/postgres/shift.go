package postgres

import (
	pbc "cash_service/genproto/cash_service_protos"
	pbo "cash_service/genproto/organization_service_protos"
	"cash_service/storage"
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5/pgxpool"
)

type shiftRepo struct {
	DB *pgxpool.Pool
}

func NewShiftRepo (db *pgxpool.Pool) storage.IShiftStorage{
	return &shiftRepo{
		DB: db,
	}
}

func (s *shiftRepo) Create(ctx context.Context,createShift *pbc.CreateShiftRequest) (*pbc.Shift, error) {
	shift := &pbc.Shift{}

	query := `INSERT INTO shifts (id, branch_id, staff_id, branch_selling_point_id, opening_time, closing_time, status)
			values ($1, $2, $3, $4, $5, $6, $7) 
		returning id, branch_id, staff_id, branch_selling_point_id, opening_time::text, closing_time::text, status::text, created_at::text `

	err := s.DB.QueryRow(ctx, query, 
		createShift.GetId(), 
		createShift.GetBranchId(),
		createShift.GetStaffId(),
		createShift.GetBranchSellingPointId(),
		createShift.GetOpeningTime(),
		createShift.GetClosingTime(),
		createShift.GetStatus(),
		).Scan(
		&shift.Id,
		&shift.BranchId,
		&shift.StaffId,
		&shift.BranchSellingPointId,
		&shift.OpeningTime,
		&shift.ClosingTime,
		&shift.Status,
 		&shift.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating shift!", err.Error())
		return &pbc.Shift{},err
	}

	return shift, nil
}

func (s *shiftRepo) Get(ctx context.Context,pKey *pbc.ShiftPrimaryKey) (*pbc.Shift, error) {
	shift := pbc.Shift{}

	query := `SELECT id, branch_id, staff_id, branch_selling_point_id, opening_time::text, closing_time::text, status::text, created_at::text, updated_at::text from shifts
				WHERE id = $1 AND deleted_at = 0 `
	err := s.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&shift.Id,
		&shift.BranchId,
		&shift.StaffId,
		&shift.BranchSellingPointId,
 		&shift.OpeningTime,
		&shift.ClosingTime,
		&shift.Status,
		&shift.CreatedAt,
		&shift.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Shift!", err.Error())
		return &pbc.Shift{}, err
	}
	return &shift, nil
}

func (s *shiftRepo) GetList(ctx context.Context, req *pbc.GetShiftListRequest) (*pbc.ShiftsResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		shifts = pbc.ShiftsResponse{}
	)

	countQuery := `SELECT count(1) from shifts where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND id ilike '%%%s%%' `, search)
	}

	err := s.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of shift!", err.Error())
		return &pbc.ShiftsResponse{},err
	}

	query := `SELECT id, branch_id, staff_id, branch_selling_point_id, opening_time::text, closing_time::text, status::text, created_at::text, updated_at::text from shifts 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND id ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := s.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pbc.ShiftsResponse{}, err
	}

	for rows.Next() {
		shift := pbc.Shift{}

		err := rows.Scan(
			&shift.Id,
			&shift.BranchId,
			&shift.StaffId,
			&shift.BranchSellingPointId,
			&shift.OpeningTime,
			&shift.ClosingTime,
			&shift.Status,
			&shift.CreatedAt,
			&shift.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning shift!",err.Error())
			return &pbc.ShiftsResponse{},err
		}

		shifts.Shifts = append(shifts.Shifts, &shift)
	}

	shifts.Count = count

	return &shifts, nil
}

func (s *shiftRepo) Update(ctx context.Context, updShift *pbc.Shift) (*pbc.Shift, error) {
	shift := pbc.Shift{}

	query := `UPDATE shifts SET branch_id = $1, staff_id = $2, branch_selling_point_id = $3, 
	opening_time = $4, closing_time = $5, status = $6,
		updated_at = NOW() 
			WHERE id = $7 AND deleted_at = 0 
		returning id, branch_id, staff_id, branch_selling_point_id, opening_time::text, closing_time::text, status::text, updated_at::text `
	err := s.DB.QueryRow(ctx, query, 
		updShift.GetBranchId(),
		updShift.GetStaffId(),
		updShift.GetBranchSellingPointId(),
		updShift.GetOpeningTime(),
		updShift.GetClosingTime(),
		updShift.GetStatus(),
		updShift.GetId(),
		).Scan(

		&shift.Id,
		&shift.BranchId,
		&shift.StaffId,
		&shift.BranchSellingPointId,
		&shift.OpeningTime,
		&shift.ClosingTime,
		&shift.Status,
 		&shift.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating shift!", err.Error())
		return &pbc.Shift{}, err
	}

	return &shift, nil

}

func (s *shiftRepo) Delete(ctx context.Context, pKey *pbc.ShiftPrimaryKey) (error)  {
	query := `UPDATE shifts SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := s.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting shift!",err.Error())
		return err
	}
	return nil
}

func (s *shiftRepo) GenerateShiftID(ctx context.Context, branchSellingPointId string, branchName *pbo.BranchNameResponse) (string, error){
	words := strings.Fields(branchName.GetName())

	var firstLetters string
	if len(words) > 1 {
		firstLetters = string(words[0][0]) + string(words[1][0])
	} else {
		firstLetters = string(words[0][0]) + string(words[0][1])
	}

	var count int
	query := `SELECT COUNT(*) FROM shifts WHERE branch_selling_point_id = $1 `

	err := s.DB.QueryRow(ctx,query, branchSellingPointId).Scan(&count)
	if err != nil {
		return "", err
	}	

 	newID := fmt.Sprintf("%s-%04d", strings.ToUpper(firstLetters), count+1)
	return newID, nil
}

func (s *shiftRepo) GetStatusByBranchSellingPointID(ctx context.Context, branchSellingPointID string) (string, error) {
	var status string

	query := `SELECT status::text from shifts where branch_selling_point_id = $1 `

	err := s.DB.QueryRow(ctx, query, branchSellingPointID).Scan(
		&status,
	)

	if err != nil{
		fmt.Println("Error while getting status of shift!", err.Error())
		return "", err
	}

	return status, nil
}

func (s shiftRepo) CloseShift(ctx context.Context, pKey *pbc.ShiftPrimaryKey) (error) {
	query := `UPDATE shifts SET status = 'closed' WHERE id = $1 `

	_, err := s.DB.Exec(ctx, query, pKey.GetId())
	if err != nil{
		fmt.Println("error while updating shift status to closed")
		return err
	}

	return nil
}