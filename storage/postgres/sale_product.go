package postgres

import (
	pb "cash_service/genproto/cash_service_protos"
	"cash_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type saleProductRepo struct {
	DB *pgxpool.Pool
}

func NewSaleProductRepo (db *pgxpool.Pool) storage.ISaleProductStorage{
	return &saleProductRepo{
		DB: db,
	}
}

func (s *saleProductRepo) Create(ctx context.Context,createSaleProduct *pb.CreateSaleProductRequest) (*pb.SaleProduct, error) {
	saleProduct := pb.SaleProduct{}

	query := `INSERT INTO sale_products (id, category_id, product_id, warehouse_product_id, sale_id, quantity, price, total_sum)
			values ($1, $2, $3, $4, $5, $6, $7, $8) 
		returning id, category_id, product_id, warehouse_product_id, sale_id, quantity, price, total_sum, created_at::text `

	uid := uuid.New()

	err := s.DB.QueryRow(ctx, query, 
		uid, 
		createSaleProduct.GetCategoryId(),
		createSaleProduct.GetProductId(),
		createSaleProduct.GetWarehouseProductId(),
		createSaleProduct.GetSaleId(),
		createSaleProduct.GetQuantity(),
		createSaleProduct.GetPrice(),
		createSaleProduct.GetTotalSum(),
		).Scan(
		&saleProduct.Id,
		&saleProduct.CategoryId,
		&saleProduct.ProductId,
		&saleProduct.WarehouseProductId,
		&saleProduct.SaleId,
		&saleProduct.Quantity,
		&saleProduct.Price,
		&saleProduct.TotalSum,
 		&saleProduct.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating saleProduct!", err.Error())
		return &pb.SaleProduct{},err
	}

	return &saleProduct, nil
}

func (s *saleProductRepo) Get(ctx context.Context,pKey *pb.SaleProductPrimaryKey) (*pb.SaleProduct, error) {
	saleProduct := pb.SaleProduct{}

	query := `SELECT id, category_id, product_id, warehouse_product_id, sale_id, quantity, price, 
		total_sum, created_at::text, updated_at::text from sale_products
				WHERE id = $1 AND deleted_at = 0 `
	err := s.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&saleProduct.Id,
		&saleProduct.CategoryId,
		&saleProduct.ProductId,
		&saleProduct.WarehouseProductId,
		&saleProduct.SaleId,
		&saleProduct.Quantity,
		&saleProduct.Price,
		&saleProduct.TotalSum,
 		&saleProduct.CreatedAt,
 		&saleProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting SaleProduct!", err.Error())
		return &pb.SaleProduct{}, err
	}
	return &saleProduct, nil
}

func (s *saleProductRepo) GetList(ctx context.Context, req *pb.GetSaleProductListRequest) (*pb.SaleProductResponse, error) {
	var(
		count int32
		offset = (req.GetPage() - 1) * req.GetLimit()
		saleProducts = pb.SaleProductResponse{}
	)

	countQuery := `SELECT count(1) from sale_products where deleted_at = 0 `

	err := s.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of saleProduct!", err.Error())
		return &pb.SaleProductResponse{},err
	}

	query := `SELECT id, category_id, product_id, warehouse_product_id, sale_id, quantity, price, 
	total_sum, created_at::text, updated_at::text from sale_products 
					WHERE deleted_at = 0 `

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := s.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.SaleProductResponse{}, err
	}

	for rows.Next() {
		saleProduct := pb.SaleProduct{}

		err := rows.Scan(
			&saleProduct.Id,
			&saleProduct.CategoryId,
			&saleProduct.ProductId,
			&saleProduct.WarehouseProductId,
			&saleProduct.SaleId,
			&saleProduct.Quantity,
			&saleProduct.Price,
			&saleProduct.TotalSum,
			&saleProduct.CreatedAt,
			&saleProduct.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning saleProduct!",err.Error())
			return &pb.SaleProductResponse{},err
		}

		saleProducts.SaleProducts = append(saleProducts.SaleProducts, &saleProduct)
	}

	saleProducts.Count = count

	return &saleProducts, nil
}

func (s *saleProductRepo) Update(ctx context.Context, updSaleProduct *pb.SaleProduct) (*pb.SaleProduct, error) {
	saleProduct := pb.SaleProduct{}

	query := `UPDATE sale_products SET category_id = $1, product_id = $2, warehouse_product_id = $3, 
		sale_id = $4, quantity = $5, price = $6, total_sum = $7,
			updated_at = NOW() 
				WHERE id = $8 AND deleted_at = 0 
		returning id, category_id, product_id, warehouse_product_id, sale_id, quantity, price, 
		total_sum, updated_at::text `
	err := s.DB.QueryRow(ctx, query, 
		updSaleProduct.GetCategoryId(),
		updSaleProduct.GetProductId(),
		updSaleProduct.GetWarehouseProductId(),
		updSaleProduct.GetSaleId(),
		updSaleProduct.GetQuantity(),
		updSaleProduct.GetPrice(),
		updSaleProduct.GetTotalSum(),
		updSaleProduct.GetId(),
		).Scan(

		&saleProduct.Id,
		&saleProduct.CategoryId,
		&saleProduct.ProductId,
		&saleProduct.WarehouseProductId,
		&saleProduct.SaleId,
		&saleProduct.Quantity,
		&saleProduct.Price,
		&saleProduct.TotalSum,
 		&saleProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating saleProduct!", err.Error())
		return &pb.SaleProduct{}, err
	}

	return &saleProduct, nil

}

func (s *saleProductRepo) Delete(ctx context.Context, pKey *pb.SaleProductPrimaryKey) (error)  {
	query := `UPDATE sale_products SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := s.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting saleProduct!",err.Error())
		return err
	}
	return nil
}