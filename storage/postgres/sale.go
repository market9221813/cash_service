package postgres

import (
	pb "cash_service/genproto/cash_service_protos"
	"cash_service/storage"
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/spf13/cast"
)

type saleRepo struct {
	DB *pgxpool.Pool
}

func NewSaleRepo (db *pgxpool.Pool) storage.ISaleStorage{
	return &saleRepo{
		DB: db,
	}
}

func (s *saleRepo) Create(ctx context.Context,createSale *pb.CreateSaleRequest) (*pb.Sale, error) {
	sale := pb.Sale{}

	query := `INSERT INTO sales (id, shift_id, branch_id, branch_selling_point_id, staff_id, status, payment_type, total_sum)
			values ($1, $2, $3, $4, $5, $6, $7, $8) 
		returning id, shift_id, branch_id, branch_selling_point_id, staff_id::text, 
			status::text, payment_type::text, total_sum, created_at::text `


	err := s.DB.QueryRow(ctx, query, 
		createSale.GetId(), 
		createSale.GetShiftId(),
		createSale.GetBranchId(),
		createSale.GetBranchSellingPointId(),
		createSale.GetStaffId(),
		createSale.GetStatus(),
		createSale.GetPaymentType(),
		createSale.GetTotalSum(),
		).Scan(
		&sale.Id,
		&sale.ShiftId,
		&sale.BranchId,
		&sale.BranchSellingPointId,
		&sale.StaffId,
		&sale.Status,
		&sale.PaymentType,
		&sale.TotalSum,
 		&sale.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating sale!", err.Error())
		return &pb.Sale{},err
	}

	return &sale, nil
}

func (s *saleRepo) Get(ctx context.Context,pKey *pb.SalePrimaryKey) (*pb.Sale, error) {
	sale := pb.Sale{}

	query := `SELECT id, shift_id, branch_id, branch_selling_point_id, staff_id::text, 
	status::text, payment_type::text, total_sum, created_at::text, updated_at::text from sales
				WHERE id = $1 AND deleted_at = 0 `
	err := s.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&sale.Id,
		&sale.ShiftId,
		&sale.BranchId,
		&sale.BranchSellingPointId,
 		&sale.StaffId,
		&sale.Status,
		&sale.PaymentType,
		&sale.TotalSum,
		&sale.CreatedAt,
		&sale.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Sale!", err.Error())
		return &pb.Sale{}, err
	}
	return &sale, nil
}

func (s *saleRepo) GetList(ctx context.Context, req *pb.GetSaleListRequest) (*pb.SalesResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		sales = pb.SalesResponse{}
	)

	countQuery := `SELECT count(1) from sales where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND id ilike '%%%s%%' `, search)
	}

	err := s.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of sale!", err.Error())
		return &pb.SalesResponse{},err
	}

	query := `SELECT id, shift_id, branch_id, branch_selling_point_id, staff_id::text, 
	status::text, payment_type::text, total_sum, created_at::text, updated_at::text from sales 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND id ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := s.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.SalesResponse{}, err
	}

	for rows.Next() {
		sale := pb.Sale{}

		err := rows.Scan(
			&sale.Id,
			&sale.ShiftId,
			&sale.BranchId,
			&sale.BranchSellingPointId,
			&sale.StaffId,
			&sale.Status,
			&sale.PaymentType,
			&sale.TotalSum,
			&sale.CreatedAt,
			&sale.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning sale!",err.Error())
			return &pb.SalesResponse{},err
		}

		sales.Sales = append(sales.Sales, &sale)
	}

	sales.Count = count

	return &sales, nil
}

func (s *saleRepo) Update(ctx context.Context, updShift *pb.Sale) (*pb.Sale, error) {
	sale := pb.Sale{}

	query := `UPDATE sales SET shift_id = $1, branch_id = $2, branch_selling_point_id = $3, 
	staff_id = $4, status = $5, payment_type = $6, total_sum = $7,
		updated_at = NOW() 
			WHERE id = $8 AND deleted_at = 0 
		returning id, shift_id, branch_id, branch_selling_point_id, staff_id::text, status::text, 
			payment_type::text, total_sum, updated_at::text `
	err := s.DB.QueryRow(ctx, query, 
		updShift.GetShiftId(),
		updShift.GetBranchId(),
		updShift.GetBranchSellingPointId(),
		updShift.GetStaffId(),
		updShift.GetStatus(),
		updShift.GetPaymentType(),
		updShift.GetTotalSum(),
		updShift.GetId(),
		).Scan(

		&sale.Id,
		&sale.ShiftId,
		&sale.BranchId,
		&sale.BranchSellingPointId,
		&sale.StaffId,
		&sale.Status,
		&sale.PaymentType,
		&sale.TotalSum,
 		&sale.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating sale!", err.Error())
		return &pb.Sale{}, err
	}

	return &sale, nil

}

func (s *saleRepo) Delete(ctx context.Context, pKey *pb.SalePrimaryKey) (error)  {
	query := `UPDATE sales SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := s.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting sale!",err.Error())
		return err
	}
	return nil
}


func (s *saleRepo) GenerateSaleID(ctx context.Context, branchSellingPointID string) (string, error) {
	currentDate := time.Now().Format("2006-01-02")

	var maxID string

	query := `SELECT COALESCE(MAX(id::text), '0') FROM sales WHERE id LIKE $1 AND branch_selling_point_id = $2`
	err := s.DB.QueryRow(ctx, query, currentDate+"%", branchSellingPointID).Scan(&maxID)
	if err != nil {
		fmt.Println("error while getting max sale id!", err.Error())
		return "", err
	}

	var maxIDInt int
	if maxID != "0" {
		maxIDInt = cast.ToInt(maxID[strings.LastIndex(maxID, "-")+1:]) // Extract the last part of the ID
	}

	maxIDInt++

	newID := fmt.Sprintf("%s-%d", currentDate, maxIDInt)
	return newID, nil
}