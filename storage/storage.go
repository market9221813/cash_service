package storage

import (
	pbc "cash_service/genproto/cash_service_protos"
	pbo "cash_service/genproto/organization_service_protos"
	"context"
)

type IStorage interface {
	Close()
	Shift() IShiftStorage
	Sale() ISaleStorage
	SaleProduct() ISaleProductStorage
}

type IShiftStorage interface {
	Create(context.Context, *pbc.CreateShiftRequest) (*pbc.Shift, error)
	Get(context.Context, *pbc.ShiftPrimaryKey) (*pbc.Shift, error)
	GetList(context.Context, *pbc.GetShiftListRequest) (*pbc.ShiftsResponse, error)
	Update(context.Context, *pbc.Shift) (*pbc.Shift, error)
	Delete(context.Context, *pbc.ShiftPrimaryKey) (error)
	GenerateShiftID(context.Context, string, *pbo.BranchNameResponse) (string, error)
	GetStatusByBranchSellingPointID(context.Context, string) (string, error)
	CloseShift(context.Context, *pbc.ShiftPrimaryKey) (error)
}

type ISaleStorage interface {
	Create(context.Context, *pbc.CreateSaleRequest) (*pbc.Sale, error)
	Get(context.Context, *pbc.SalePrimaryKey) (*pbc.Sale, error)
	GetList(context.Context, *pbc.GetSaleListRequest) (*pbc.SalesResponse, error)
	Update(context.Context, *pbc.Sale) (*pbc.Sale, error)
	Delete(context.Context, *pbc.SalePrimaryKey) (error)
	GenerateSaleID(context.Context, string) (string,error)
}

type ISaleProductStorage interface {
	Create(context.Context, *pbc.CreateSaleProductRequest) (*pbc.SaleProduct, error)
	Get(context.Context, *pbc.SaleProductPrimaryKey) (*pbc.SaleProduct, error)
	GetList(context.Context, *pbc.GetSaleProductListRequest) (*pbc.SaleProductResponse, error)
	Update(context.Context, *pbc.SaleProduct) (*pbc.SaleProduct, error)
	Delete(context.Context, *pbc.SaleProductPrimaryKey) (error)
}

