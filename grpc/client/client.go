package client

import (
	"cash_service/config"
	cash_service "cash_service/genproto/cash_service_protos"
	organization_service "cash_service/genproto/organization_service_protos"
	"fmt"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	ShiftService() cash_service.ShiftServiceClient
	SaleService() cash_service.SaleServiceClient
	SaleProductService() cash_service.SaleProductServiceClient

	// Organization service

	BranchService() organization_service.BranchServiceClient
}

type grpcClients struct {
	shiftService cash_service.ShiftServiceClient
	saleService cash_service.SaleServiceClient
	saleProductService cash_service.SaleProductServiceClient

	branchService organization_service.BranchServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connCashService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting cash_service!",err.Error())
		return nil, err
	}

	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceGrpcHost+cfg.OrganizationServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting organization_service!",err.Error())
		return nil, err
	}

	return &grpcClients{ 
		shiftService: cash_service.NewShiftServiceClient(connCashService),
		saleService: cash_service.NewSaleServiceClient(connCashService),
		saleProductService: cash_service.NewSaleProductServiceClient(connCashService),

		branchService: organization_service.NewBranchServiceClient(connOrganizationService),
	}, nil
}

func (g *grpcClients) ShiftService() cash_service.ShiftServiceClient{
	return g.shiftService
}

func (g *grpcClients) SaleService() cash_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() cash_service.SaleProductServiceClient {
	return g.saleProductService
}

// Organization service

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}
 