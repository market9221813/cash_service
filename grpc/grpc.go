package grpc

import (
	pb "cash_service/genproto/cash_service_protos"
	"cash_service/grpc/client"
	"cash_service/service"
	"cash_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterShiftServiceServer(grpcServer, service.NewShiftService(strg, services))
	pb.RegisterSaleServiceServer(grpcServer, service.NewSaleService(strg, services))
	pb.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(strg, services))
	reflection.Register(grpcServer)

	return grpcServer
}