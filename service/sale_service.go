package service

import (
	pb "cash_service/genproto/cash_service_protos"
	"cash_service/grpc/client"
	"cash_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type saleService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedSaleServiceServer
}

func NewSaleService (storage storage.IStorage, services client.IServiceManager) *saleService{
	return &saleService{
		storage: storage,
		services: services,
	}
}

func (s *saleService) Create(ctx context.Context, req *pb.CreateSaleRequest) (*pb.Sale,error) {
	newID, err := s.storage.Sale().GenerateSaleID(ctx, req.BranchSellingPointId)
	if err != nil{
		fmt.Println("Error in service, while generating sale id!",err.Error())
		return &pb.Sale{},err
	}

	req.Id = newID

	sale, err := s.storage.Sale().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating sale!", err.Error())
		return &pb.Sale{},err
	}
	return sale, nil
}

func (s *saleService) Get(ctx context.Context, req *pb.SalePrimaryKey) (*pb.Sale,error) {
	sale, err := s.storage.Sale().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting sale!", err.Error())
		return &pb.Sale{},err
	}
	return sale, nil
}

func (s *saleService) GetList(ctx context.Context, req *pb.GetSaleListRequest) (*pb.SalesResponse,error) {
	sales, err := s.storage.Sale().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting sale list!",err.Error())
		return &pb.SalesResponse{},err
	}
	return sales, nil
}

func (s *saleService) Update(ctx context.Context, req *pb.Sale) (*pb.Sale, error) {
	sale, err := s.storage.Sale().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating sale!",err.Error())
		return &pb.Sale{}, err
	}
	return sale, nil
}

func (s *saleService) Delete(ctx context.Context, req *pb.SalePrimaryKey) (*emptypb.Empty,error) {
	err := s.storage.Sale().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting sale!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}