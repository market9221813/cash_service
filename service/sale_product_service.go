package service

import (
	pb "cash_service/genproto/cash_service_protos"
	"cash_service/grpc/client"
	"cash_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type saleProductService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedSaleProductServiceServer
}

func NewSaleProductService (storage storage.IStorage, services client.IServiceManager) *saleProductService{
	return &saleProductService{
		storage: storage,
		services: services,
	}
}

func (s *saleProductService) Create(ctx context.Context, req *pb.CreateSaleProductRequest) (*pb.SaleProduct,error) {
	saleProduct, err := s.storage.SaleProduct().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating sale product!", err.Error())
		return &pb.SaleProduct{},err
	}
	return saleProduct, nil
}

func (s *saleProductService) Get(ctx context.Context, req *pb.SaleProductPrimaryKey) (*pb.SaleProduct,error) {
	saleProduct, err := s.storage.SaleProduct().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting sale product!", err.Error())
		return &pb.SaleProduct{},err
	}
	return saleProduct, nil
}

func (s *saleProductService) GetList(ctx context.Context, req *pb.GetSaleProductListRequest) (*pb.SaleProductResponse,error) {
	saleProducts, err := s.storage.SaleProduct().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting sale product list!",err.Error())
		return &pb.SaleProductResponse{},err
	}
	return saleProducts, nil
}

func (s *saleProductService) Update(ctx context.Context, req *pb.SaleProduct) (*pb.SaleProduct, error) {
	saleProduct, err := s.storage.SaleProduct().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating sale product!",err.Error())
		return &pb.SaleProduct{}, err
	}
	return saleProduct, nil
}

func (s *saleProductService) Delete(ctx context.Context, req *pb.SaleProductPrimaryKey) (*emptypb.Empty,error) {
	err := s.storage.SaleProduct().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting sale product!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}