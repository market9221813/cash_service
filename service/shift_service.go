package service

import (
	pbc "cash_service/genproto/cash_service_protos"
	pbo "cash_service/genproto/organization_service_protos"
	"cash_service/grpc/client"
	"cash_service/storage"
	"context"
	"errors"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type shiftService struct {
	storage storage.IStorage
	services client.IServiceManager
	pbc.UnimplementedShiftServiceServer
}

func NewShiftService (storage storage.IStorage, services client.IServiceManager) *shiftService{
	return &shiftService{
		storage: storage,
		services: services,
	}
}

func (s *shiftService) Create(ctx context.Context, req *pbc.CreateShiftRequest) (*pbc.Shift,error) {
	branchName, err := s.services.BranchService().GetNameByID(ctx, &pbo.BranchPrimaryKey{Id: req.BranchId})
	if err != nil{
		fmt.Println("Error in service, while getting name of branch by id!",err.Error())
		return &pbc.Shift{},err
	}

	newID, err := s.storage.Shift().GenerateShiftID(ctx, req.BranchSellingPointId, branchName)
	if err != nil{
		fmt.Println("Error in service while genrating shift id!",err.Error())
		return &pbc.Shift{},err
	}

	req.Id = newID

	shift, err := s.storage.Shift().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating shift!", err.Error())
		return &pbc.Shift{},err
	}
	return shift, nil
}

func (s *shiftService) Get(ctx context.Context, req *pbc.ShiftPrimaryKey) (*pbc.Shift,error) {
	shift, err := s.storage.Shift().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting shift!", err.Error())
		return &pbc.Shift{},err
	}
	return shift, nil
}

func (s *shiftService) GetList(ctx context.Context, req *pbc.GetShiftListRequest) (*pbc.ShiftsResponse,error) {
	shifts, err := s.storage.Shift().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting shift list!",err.Error())
		return &pbc.ShiftsResponse{},err
	}
	return shifts, nil
}

func (s *shiftService) Update(ctx context.Context, req *pbc.Shift) (*pbc.Shift, error) {
	shift, err := s.storage.Shift().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating shift!",err.Error())
		return &pbc.Shift{}, err
	}
	return shift, nil
}

func (s *shiftService) Delete(ctx context.Context, req *pbc.ShiftPrimaryKey) (*emptypb.Empty,error) {
	err := s.storage.Shift().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting shift!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

func (s *shiftService) OpenShift(ctx context.Context, req *pbc.CreateShiftRequest) (*pbc.Shift, error) {
 
	shiftStatus, err := s.storage.Shift().GetStatusByBranchSellingPointID(ctx,req.BranchSellingPointId)
	if err != nil{
		fmt.Println("Service, creating a new shift!")
	}
 
	if shiftStatus == "opened"{
		fmt.Println("Error in service, you have the opened shift!")
		return &pbc.Shift{}, errors.New("close the opened shift before opening a new one")
	}

	shift, err := s.services.ShiftService().Create(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while opening shift!",err.Error())
		return &pbc.Shift{},err
	}
 
	return shift, nil
}

func (s *shiftService) CloseShift(ctx context.Context, req *pbc.ShiftPrimaryKey) (*emptypb.Empty,error) {
	err := s.storage.Shift().CloseShift(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while updating shift status to closed!")
	}
	return nil, err
}