CREATE TYPE shift_status_enum AS ENUM (
    'opened',
    'closed'
);

CREATE TYPE sale_status_enum AS ENUM (
    'in_process',
    'completed'
);

CREATE TYPE sales_payment_type_enum AS ENUM (
    'cash',
    'uzcard',
    'payme',
    'click',
    'humo',
    'apelsin'
);

CREATE TABLE IF NOT EXISTS shifts (
    id varchar(10) primary key,
    branch_id uuid,
    staff_id uuid,
    branch_selling_point_id varchar(7),
    opening_time timestamp,
    closing_time timestamp,
    status shift_status_enum,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS sales (
    id varchar(20),
    shift_id varchar(10) references shifts(id),
    branch_id uuid,
    branch_selling_point_id varchar(7),
    staff_id uuid,
    status sale_status_enum,
    payment_type sales_payment_type_enum,
    total_sum float,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS sale_products (
    id uuid primary key,
    category_id uuid,
    product_id uuid,
    warehouse_product_id uuid,
    sale_id varchar(20),
    product_selling_id uuid,
    quantity int,
    price float,
    total_sum float,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

 